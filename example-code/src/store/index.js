import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { reject } from 'q';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    filterProduts: [],
    filterActive: false,
    currentProduct: {},
    cart: []
  },
  mutations: {
    setProducts (state, payload) {
      state.products = payload;
    },
    setFilterProducts (state, payload) {
      state.filterActive = true;
      state.filterProduts = payload;
    },
    setCurrentProduct(state, payload) {
      state.currentProduct = payload;
    },
    setCart(state, payload) {
      state.cart.push(payload);
    }
  },
  actions: {
    loadProducts({ commit }) {
      return new Promise((resolve, reject) => {
        axios.get('http://localhost:3000/products')
        .then(response => {
          const products = response.data;
          commit('setProducts', products)
          resolve(products)
        });
        
      })
    },
    loadCurrentProduct({state, commit}, payload) {
      let currentProduct = state.products.find(function(item){return item.id == payload;});
      commit('setCurrentProduct', currentProduct)
    },
    addCart({state, commit}, payload) {
      let product = {...payload};
      let newProduct = state.cart.find(function(item){return item.id == product.id;});
      if(!newProduct) {
        product.count = 1;
        commit('setCart', product)
      } else {
        newProduct.count += 1;
        let indexProduct = state.cart.findIndex((n) => {
          return n.id == newProduct.id;
        });
        state.cart.splice(indexProduct, 1, newProduct);
      }
    }
  },
  getters: {
    getProducts(state) {
      return state.products;
    },
    getFilterProducts(state) {
      if(state.filterActive) {
        return state.filterProduts;
      } else {
        return state.products;
      }
    },
    getMainProducts(state) {
      return state.products.slice(0, 4)
    },
    getCurrentProduct(state) {
      return state.currentProduct;
    },
    getCart(state) {
      return state.cart;
    }
  }
})
